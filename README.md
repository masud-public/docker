* The final directory structure is as follows.

```
.
├── Makefile
├── README.md
├── docker/
├── docker-compose.yml
└── your_project/
```

## clone

```
$ git@gitlab.com:masud.alzaf1/docker.git
$ cd docker
```


## how to clone 

* Clone it under the docker directory.

## Initialization

0. If host OS is Linux, you will need to do the following:

* Remove the directory name from all log files in the database configuration file.
  * Edit the file ./docker/appetizer_db/custom.cnf

```
log-error = mysql-error.log
slow_query_log_file = mysql-slow.log
general_log_file = mysql-general.log
```

0.1 For M1 Mac user.

If your computer is an M1 Mac, you need to add the following to the last line of apache2.conf in your_project.

```
Mutex posixsem
```

1. Docker setting

```
$ make init
```

2. Setting file copy

(Change directory to "docker" in terminal)

if .env file exists and you don't want to overwrite, you don't need to do the following.

```

cd your_project
git fetch
git checkout develop
git pull origin develop
cp local.env .env
cd ..
```

3. Laravel key generate

```
make laravel-key-generate-all
```

4. yarn install

```
make yarn-install-all
```

## Docker run.

```
$ make up
```


check this url.  

- API BASE ENDPOINT
http://localhost:8180/
