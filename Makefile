up:
	@docker compose up -d;
	@make ps

stop:
	@docker compose stop;

init:
	@echo "Welcome";
	@make down
	@docker compose build;
	@make up
	@make composer-install;
	@make laravel-key-generate;

down:
	@docker compose down;

build:
	@docker compose build --no-cache;

ps:
	@docker compose ps;

migrate:
	@docker compose exec project_dir php artisan migrate;

migrate-fresh:
	@docker compose exec project_dir php artisan migrate:fresh;
	
migrate-fresh-seed:
	@docker compose exec project_dir php artisan migrate:fresh --seed;

composer-install:
	@docker compose exec project_dir composer install;

laravel-key-generate:
	@docker compose exec project_dir php artisan key:generate;

laravel-cache-clear:
	@docker compose exec project_dir php artisan route:clear;
	@docker compose exec project_dir php artisan view:clear;
	@docker compose exec project_dir php artisan config:clear;
	@docker compose exec project_dir php artisan optimize:clear;


# yarn-install:
# 	@docker compose exec project_dir yarn install;
# front-build:
# 	@docker compose exec project_dir yarn build;
# front-dev:
# 	@docker compose exec project_dir yarn dev;


# init-db:
# 	docker compose exec nexzan_db sh -c "mysql -u root --password='root!password?' -e 'drop database nexzandb' || true"
# 	docker compose exec nexzan_db sh -c "mysql -u root --password='root!password?' < /docker-entrypoint-initdb.d/0.create_db.sql || true"
# 	docker compose exec nexzan_db sh -c "mysql -u root --password='root!password?' < /docker-entrypoint-initdb.d/nexzan_db.sql"